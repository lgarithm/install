```bash
curl https://gitlab.com/lgarithm/install/raw/master/bootstrap.sh | sh
```

Or

```bash
wget -qO- https://gitlab.com/lgarithm/install/raw/master/bootstrap.sh | sh
```
