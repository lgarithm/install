#!/bin/sh
set -e

cd $(dirname $0)

./install-cuda.sh
./install-cudnn7.sh
./install-docker.sh
