#!/bin/sh

set -e

cd $(dirname $0)

sudo cp sources.list.aliyun /etc/apt/sources.list
sudo apt update
sudo apt install -y openssh-server git apt-transport-https curl gnupg-curl net-tools
