# https://docs.microsoft.com/en-us/azure/virtual-machines/linux/n-series-driver-setup
set -e

URL_PREFIX=https://developer.download.nvidia.com/compute/cuda/repos/ubuntu1810/x86_64
CUDA_REPO_PKG=cuda-repo-ubuntu1810_10.1.105-1_amd64.deb

wget -O /tmp/${CUDA_REPO_PKG} ${URL_PREFIX}/${CUDA_REPO_PKG}
sudo dpkg -i /tmp/${CUDA_REPO_PKG}
sudo apt-key adv --fetch-keys ${URL_PREFIX}/7fa2af80.pub

sudo apt update
sudo apt install -y cuda-drivers cuda

sudo apt install -y nvidia-cuda-dev # for cuda_runtime.h
