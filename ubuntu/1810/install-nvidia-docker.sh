#!/bin/sh
set -e

curl -s -L https://nvidia.github.io/nvidia-docker/gpgkey | sudo apt-key add -
distribution=ubuntu18.04
SOURCES_LIST=https://nvidia.github.io/nvidia-docker/$distribution/nvidia-docker.list
echo $SOURCES_LIST
curl -s -L $SOURCES_LIST |
    sudo tee /etc/apt/sources.list.d/nvidia-docker.list
sudo apt update
sudo apt install -y nvidia-docker2
