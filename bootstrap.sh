#!/bin/sh

set -e

prepare_wget() {
    cd $HOME/Downloads
    local URL=http://gitlab.com/lgarithm/install/-/archive/master/install-master.tar.bz2
    if [ -f install-master.tar.bz2 ]; then
        rm install-master.tar.bz2
    fi
    wget ${URL}
    tar -xf install-master.tar.bz2
    cd install-master
}

prepare_git() {
    cd $HOME/Downloads
    local URL=http://gitlab.com/lgarithm/install.git
    if [ -d install ]; then
        rm -fr install
    fi
    git clone $URL
    cd install
}

prepare() {
    if [ $(which git | wc -l) -gt 0 ]; then
        prepare_git
    else
        prepare_wget
    fi
}

prepare

if [ -f /etc/os-release ]; then
    . /etc/os-release
fi

# TODO: check os type
if [ "$VERSION_ID" = "16.04" ]; then
    ./ubuntu/1604/bootstrap.sh
elif [ "$VERSION_ID" = "18.04" ]; then
    ./ubuntu/1804/bootstrap.sh
elif [ "$VERSION_ID" = "18.10" ]; then
    ./ubuntu/1810/bootstrap.sh
else
    echo "unknown os"
fi
